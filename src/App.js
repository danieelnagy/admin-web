import logo from './logo.svg';
import './App.css';

import DashBoard from './Components/DashBoard';


function App() {


  return (

    <div className="container">

      <DashBoard />
    </div>
  );

}

export default App;