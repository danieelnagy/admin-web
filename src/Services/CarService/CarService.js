import axios from 'axios';


const API_GET_CARS = "http://localhost:8088/api/v1/cars"
const API_CREATE_CAR = "http://localhost:8088/api/v1/addcar"
const API_GET_CAR_ID = "http://localhost:8088/api/v1/findcarById/"
const API_DELETE_CAR_ID = "http://localhost:8088/api/v1/deletecar"
const API_UPDATE_CAR_ID = "http://localhost:8088/api/v1/updatecar" 

class CarService {

    getCars() {
        
        return axios.get(API_GET_CARS, {
          auth: {
            username: 'admin',
            password: 'admin'
          }
        });
    }

    

  createCar(car){
      axios.post(API_CREATE_CAR, {
        name : car.name,
        status: true,
        color: car.color,
        carType: {
            "id": car.carTypeId
        }   
      }, {
        auth: {
          username: 'admin',
          password: 'admin'
        }
      });
      return window.location.href='/home';
  }




  updateCar(car, carId){
    axios.put(API_UPDATE_CAR_ID + '/' + carId, car, {
      auth: {
        username: 'admin',
        password: 'admin'
      }
    });
    return window.location.href='/home';
}

  getCarById(carId){
      return axios.get(API_GET_CAR_ID + carId, {
        auth: {
          username: 'admin',
          password: 'admin'
        }
      })
  }





  deleteCar(carId){
      axios.delete(API_DELETE_CAR_ID + '/' + carId, {
        auth: {
          username: 'admin',
          password: 'admin'
        }
      });
      }

  

}

export default new CarService() 