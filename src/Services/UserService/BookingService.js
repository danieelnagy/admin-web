import axios from 'axios';



const API_GETALL_MYBOOKING = "http://localhost:8088/api/v1/myorders"
const API_CREATE_BOOKING = "http://localhost:8088/api/v1/ordercar"
const API_GET_BOOKING_ID = "http://localhost:8088/api/v1/findOrderById"
const API_DELETE_CAR_ID = "http://localhost:8088/api/v1/deletecar"
const API_UPDATE_BOOKING_ID = "http://localhost:8088/api/v1/updateorder" 

class CustomerService {

    getMyBookings() {
        
        return axios.get(API_GETALL_MYBOOKING, {
            // Axios looks for the `auth` option, and, if it is set, formats a
            // basic auth header for you automatically.
            auth: {
              username: 'admin',
              password: 'admin'
            }
          });
    
    }




    createBooking(booking){
        axios.post(API_CREATE_BOOKING, booking, {
          auth: {
            username: 'admin',
            password: 'admin'
          }
        })
        return window.location.href='/myorders';
    }


    AddLocalisations (localisation_id, localisations) {
      let url = `http://127.0.0.1:8000/api/localisations/${localisation_id}/sessions`; //template literals
      axios.request({
          method: 'post',
          url: url,
          data: localisations,
          }).catch(err => console.log(err))
     }
  
  
  
  
    updateBooking(booking, bookingId){
      axios.put(API_UPDATE_BOOKING_ID  + '/' + bookingId,  booking, {
        // Axios looks for the `auth` option, and, if it is set, formats a
        // basic auth header for you automatically.
        auth: {
          username: 'admin',
          password: 'admin'
        }
      });
      return window.location.href='/myorders';
  
  }

  
  
    getBookingById(id){
        return axios.get(API_GET_BOOKING_ID + '/' + id, {
          auth: {
            username: 'admin',
            password: 'admin'
          }
        })
    }
  
    deleteCar(carId){
        axios.delete(API_DELETE_CAR_ID + '/' + carId, {
          auth: {
            username: 'admin',
            password: 'admin'
          }
        })
        }

}

export default new CustomerService() 