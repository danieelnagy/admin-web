import axios from 'axios';


const API_GET_USER_URL = "http://localhost:8088/api/v1/findAllUser"
const API_GET_USER_ID = "http://localhost:8088/api/v1/findUserById"

class CustomerService {

    getCustomers() {
        
        return axios.get(API_GET_USER_URL, {
            auth: {
              username: 'admin',
              password: 'admin'
            }
          });
    }

    getUserById(id){
      return axios.get(API_GET_USER_ID + '/' + id, {
        auth: {
          username: 'admin',
          password: 'admin'
        }
      });
  }

}

export default new CustomerService() 