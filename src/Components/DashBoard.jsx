import React, { Component } from 'react';
import CarComponent from './CarComponent';
import NavigationBar from "./NavigationBar";
import Footer from './Footer';
import AuthenticatedRoute from './AuthenticatedRoute';
import Login from './Login';
import UpdateCarComponent from './UpdateCarComponent';
import AddCarComponent from './AddCarComponent';
import CustomerComponent from './CustomerComponent';


import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom';

class dashboard extends Component {
    render() {
        return (
            <div>
                <Router>
                    <NavigationBar />
                    <div className="container">
                        <Switch>
                            <Route path="/" exact component={Login} />
                            <Route path="/login" exact component={Login} />
                            <AuthenticatedRoute path='/customers' component={CustomerComponent} />
                            <AuthenticatedRoute path='/addcar' component={AddCarComponent} />
                                <AuthenticatedRoute path='/editcar/:id' component={UpdateCarComponent} />

                            <AuthenticatedRoute path='/Home' component={CarComponent} />
                            <AuthenticatedRoute render={() => <h1>Not found!</h1>} />
                        </Switch>
                    </div>
                    <Footer />
                </Router>

            </div>
        );
    }
}

export default dashboard;

