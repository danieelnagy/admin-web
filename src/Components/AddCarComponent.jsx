import React, { Component } from 'react'
import CarService from '../Services/CarService/CarService';
import "./../Style.css";

class AddCarComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
                
            name: '',
            color: '',
            carTypeId: ''
        }

        this.changeNameHandler = this.changeNameHandler.bind(this);
        this.changeColorHandler = this.changeColorHandler.bind(this);
        this.changeCarTypeHandler = this.changeCarTypeHandler.bind(this);
        this.saveCar = this.saveCar.bind(this);

    }

    saveCar = (e) => {
        e.preventDefault();
        let car = {name: this.state.name, color: this.state.color, carTypeId: this.state.carTypeId};
        CarService.createCar(car);
    }
    
    changeNameHandler= (event) => {
        this.setState({name: event.target.value});
    }

    changeColorHandler= (event) => {
        this.setState({color: event.target.value});
    }

    changeCarTypeHandler= (event) => {
        this.setState({carTypeId: event.target.value});
    }

    cancel(){
        this.props.history.push('/');
    }


    render() {
        return (
<div>
                <br/>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                <h1>AddCar</h1>
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> Name: </label>
                                            <input placeholder="name" name="name" className="form-control" 
                                                value={this.state.name} onChange={this.changeNameHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Color: </label>
                                            <input placeholder="color" name="color" className="form-control" 
                                                value={this.state.color} onChange={this.changeColorHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Type(1-3): </label>
                                            <input placeholder="typeID" name="carTypeId" className="form-control" 
                                                value={this.state.carTypeId} onChange={this.changeCarTypeHandler}/>
                                        </div>

                                        <button className="btn btn-success" onClick={this.saveCar}>Save</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
        )
    }
}

export default AddCarComponent
