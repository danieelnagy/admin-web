import React, { Component } from 'react'
import CarService from '../Services/CarService/CarService';
import "./../Style.css";


class UpdateCarComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {    
            id: this.props.match.params.id,            
            name: '',
            color: ''
        }

        this.changeNameHandler = this.changeNameHandler.bind(this);
        this.changeColorHandler = this.changeColorHandler.bind(this);
        this.UpdateCar = this.UpdateCar.bind(this);

    }

    UpdateCar = (e) => {
        e.preventDefault();
        let car = {name: this.state.name, color: this.state.color};
        CarService.updateCar(car, this.state.id);
    }
    
    changeNameHandler= (event) => {
        this.setState({name: event.target.value});
    }

    changeColorHandler= (event) => {
        this.setState({color: event.target.value});
    }

    cancel(){
        this.props.history.push('/home');
    }


    render() {
        return (
<div>
                <br/>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                <h1>Update car</h1>
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> Name: </label>
                                            <input placeholder="name" name="name" className="form-control" 
                                                value={this.state.name} onChange={this.changeNameHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> color: </label>
                                            <input placeholder="color" name="color" className="form-control" 
                                                value={this.state.color} onChange={this.changeColorHandler}/>
                                        </div>

                                        <button className="btn btn-success" onClick={this.UpdateCar}>Update</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
        )
    }
}

export default UpdateCarComponent
